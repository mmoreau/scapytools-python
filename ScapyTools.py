#!/usr/bin/python3

try:
	import ipaddress
	import netifaces
	from scapy.all import *
except ModuleNotFoundError as error:
	print(error)


class ScapyTools:


	@classmethod
	def ping(cls, ip):

		if isinstance(ip, str):

			rep = sr1(IP(dst=ip, ttl=1) / ICMP(), timeout=0.1, verbose=0)
			
			if rep is None:
				return 0
			else:
				if rep[ICMP].type == 0: # echo-reply
					return 1
				else:
					return 0



	@classmethod
	def portSyn(cls, ip, port):


		lock = 0


		if isinstance(ip, str):
			lock += 1


		if isinstance(port, int):
			if port in range(1, 65536):
				lock += 1


		if lock == 2:

			rep = sr1(IP(dst=ip) / TCP(sport=RandShort(), dport=port, flags="S"), timeout=0.1, verbose=0)

			if rep is None:
				return 0
			else:
				if rep[TCP].flags == 18: 
					return 1
				else:
					return 0



	@classmethod
	def getHostname(cls, interface, gateway, ip):

		"""Retrieves machine names from the network or subnetwork."""

		lock = 0

		if isinstance(interface, str):
			if interface in netifaces.interfaces():
				lock += 1


		if isinstance(gateway, str):
			try:
				ipaddress.ip_address(gateway)
				lock += 1
			except:
				pass


		if isinstance(ip, str):
			try:
				ipaddress.ip_address(str(ipaddress.ip_interface(ip)).split("/")[0])		
				lock += 1
			except:
				pass
		

		if lock == 3:

			version = ipaddress.ip_address(str(ipaddress.ip_interface(ip)).split("/")[0]).version

			for addr in [ipaddress.IPv4Network(ip) if version == 4 else ipaddress.IPv6Network(ip)][0]:

				iprev = ipaddress.ip_address(addr).reverse_pointer

				res = sr1(IP(dst=gateway) / UDP() / DNS(rd=1, qd=DNSQR(qname=iprev, qtype="PTR")), iface=interface, verbose=0)

				if res and res[DNS].an:
					print(addr, res[DNS].an.rdata.decode()[:-1])

		



	@classmethod
	def getMac(cls, ip):

		if isinstance(ip, str):

			req = srp(Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(op=1, pdst=ip), timeout=0.15, verbose=0)[0]

			if req:
				return (ip, req[Ether][0][1].src)

	

	@classmethod
	def getDNSSite(cls, name):

		"""Retrieves sites visited during DNS requests."""

		def callback_sniff(pkt):

			if pkt.haslayer(DNS):

				qname = pkt[DNS].qd.qname
				dnsan = pkt[DNS].an

				if qname:
					print("\tQNAME :", (qname.decode(), datetime.now().strftime("%d/%m/%Y - %H:%M:%S,%f")))

				if dnsan:
					[print("\tDNSAN :", (data.rrname.decode(), data.rdata, datetime.now().strftime("%d/%m/%Y - %H:%M:%S,%f"))) for data in dnsan]

				print("\n", "-"*70, "\n")


		if isinstance(name, str):
			if name in netifaces.interfaces():
				sniff(iface=name, prn=callback_sniff)