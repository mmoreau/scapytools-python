## ScapyTools

This project is under development, errors may be encountered

## Dependencies

* Python 3 (https://www.python.org/downloads/release/python-372/)

### Windows 

<pre>
    pip -r requirements.txt
</pre>

### Linux & Mac 

<pre>
    pip3.7 -r requirements.txt OR pip3 -r requirements.txt
</pre>

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/